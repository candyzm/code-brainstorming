package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 35. 搜索插入位置
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。
 * 如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 * 请必须使用时间复杂度为 O(log n) 的算法。
 *
 * 示例 1:
 * 输入: nums = [1,3,5,6], target = 5
 * 输出: 2
 *
 * 示例 2:
 * 输入: nums = [1,3,5,6], target = 2
 * 输出: 1
 *
 * 示例 3:
 * 输入: nums = [1,3,5,6], target = 7
 * 输出: 4
 */

public class Easy35 {
    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 6};
        int target = 5;
        int index = searchInsert(nums, target);
        System.out.println("index= " + index);
    }

    public static int searchInsert(int[] nums, int target) {
        // 如果目标值不存在于数组中，返回它将会被按顺序插入的位置，插入位置有 4 种情况
        // 首先考虑不存在数组中的边界情况
        if (target < nums[0]) {
            return 0;
        }

        if (target > nums[nums.length - 1]) {
            return nums.length;
        }

        // 在考虑数组内部比较的情况
        int left = 0;
        int right = nums.length - 1;
        int mid = 0;
        while (left <= right) {
            mid = left + ((right - left) >> 1);
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                return mid;
            }
        }

        // 此时单独判断是插入到做后一个元素的左边还是右边
        // 1. 当 nums[mid] > target  =>  target 应该插入在 mid 左边 => 下标即为 mid
        // 2. 当 nums[mid] < target  =>  target 应该插入在 mid 右边 => 下标即为 mid + 1
        return nums[mid] > target ? mid : (mid + 1);
    }
}
