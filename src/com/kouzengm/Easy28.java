package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 28. 找出字符串中第一个匹配项的下标
 */

public class Easy28 {
    public static void main(String[] args) {

    }

    public int strStr(String haystack, String needle) {
        // KMP 算法

        // 获得 next 数组
        int[] next = getKMPNext(needle);

        for (int i = 0, j = 0; i < haystack.length(); ++i) {

            while (j > 0 && haystack.charAt(i) != needle.charAt(j)) {
                j = next[j - 1];
            }

            if (haystack.charAt(i) == needle.charAt(j)) {
                ++j;
            }

            if (j == needle.length()) {
                return i - j + 1;
            }

        }

        return -1;
    }

    public int[] getKMPNext(String s) {
        int[] next = new int[s.length()];
        next[0] = 0;

        for (int i = 1, j = 0; i < s.length(); ++i) {
            while (j > 0 && s.charAt(i) != s.charAt(j)) {
                j = next[j - 1];
            }

            if (s.charAt(i) == s.charAt(j)) {
                ++j;
            }

            next[i] = j;
        }
        return next;
    }
}
