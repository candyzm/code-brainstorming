package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 450.删除二叉搜索树中的节点
 */

public class Mid450 {
    public TreeNode deleteNode(TreeNode root, int key) {
        if (root == null) return root;
        if (root.val > key) {
            root.left = deleteNode(root.left, key);
            return root;
        }
        if (root.val < key) {
            root.right = deleteNode(root.right, key);
            return root;
        }
        if (root.val == key) {
            if (root.left == null && root.right == null) {
                return null;
            }
            if (root.left == null) {
                return root.right;
            }
            if (root.right == null) {
                return root.left;
            }
            TreeNode res = root.right;
            while (res.left != null) {
                res = res.left;
            }
            root.right = deleteNode(root.right, res.val);
            res.right = root.right;
            res.left = root.left;
            return res;
        }
        return root;
    }
    public static void main(String[] args) {

    }
}
