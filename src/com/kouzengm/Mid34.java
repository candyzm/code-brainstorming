package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 34. 在排序数组中查找元素的第一个和最后一个位置
 *
 * 给你一个按照非递减顺序排列的整数数组 nums，和一个目标值 target。
 * 请你找出给定目标值在数组中的开始位置和结束位置。
 *
 * 如果数组中不存在目标值 target，返回 [-1, -1]。
 *
 * 你必须设计并实现时间复杂度为 O(log n) 的算法解决此问题。
 *
 * 示例 1：
 * 输入：nums = [5,7,7,8,8,10], target = 8
 * 输出：[3,4]
 *
 * 示例 2：
 * 输入：nums = [5,7,7,8,8,10], target = 6
 * 输出：[-1,-1]
 *
 * 示例 3：
 * 输入：nums = [], target = 0
 * 输出：[-1,-1]
 *
 * 提示：
 *
 *     0 <= nums.length <= 105
 *     -109 <= nums[i] <= 109
 *     nums 是一个非递减数组
 *     -109 <= target <= 109
 */

public class Mid34 {
    public static void main(String[] args) {
        int[] nums = {5, 7, 7, 8, 8, 10};
        int target = 0;
        int[] results = searchRange(nums, target);
        for (int result : results) {
            System.out.print(result + " ");
        }

    }

    public static int[] searchRange(int[] nums, int target) {
        // 这个题 0 <= nums.length <= 105 因此可以先进行边界处理
        if (nums == null || nums.length == 0) {
            return new int[] {-1, -1};
        }

        // 当 nums 不为空且长度大于 0 时
        // 先判断 target 是否在数组中
        int res = binarySearch(nums, target);
        if (res == -1) {
            // target 不在数组中, 则直接返回 [-1, -1]
            return new int[] {-1, -1};
        }

        // 若存在
        int left = res;
        int right = res;
        // 左边滑动寻找左边界
        while (left > 0 && nums[left - 1] == nums[left]) {
            left--;
        }
        // 右边滑动寻找右边界
        while (right < nums.length - 1 && nums[right] == nums[right + 1]) {
            right++;
        }
        return new int[] {left, right};
    }

    public static int binarySearch(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        // 到底是 < 还是 <= 要注意!! 左闭右闭 left == right 依然有效
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
