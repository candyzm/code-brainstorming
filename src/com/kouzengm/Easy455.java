package com.kouzengm;

import java.util.Arrays;

/**
 * @author 夕桐
 * @version 1.0
 * 455. 分发饼干
 */

public class Easy455 {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        int count = 0;
        // 饼干不动胃口动
        int j = g.length - 1;
        int i = s.length - 1;
        while (i >= 0 && j >= 0) {
            // 饼干能够满足胃口
            if (s[i] >= g[j]) {
                ++count;
                --i;
            }
            --j;
        }

        return count;
    }
    public static void main(String[] args) {

    }
}
