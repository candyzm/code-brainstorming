package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 538.把二叉搜索树转换为累加树
 */

public class Mid538 {
    int sum = 0;
    public TreeNode convertBST(TreeNode root) {
        if (root == null) return root;
        convertBST(root.right);
        sum += root.val;
        root.val = sum;
        convertBST(root.left);
        return root;
    }
    public static void main(String[] args) {

    }
}
