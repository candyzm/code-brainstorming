package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 135. 分发糖果
 */

public class Hard135 {
    public int candy(int[] ratings) {
        int[] candy = new int[ratings.length];
        candy[0] = 1;
        for (int i = 1; i < candy.length; ++i) {
            candy[i] = ratings[i] > ratings[i - 1] ? candy[i - 1] + 1: 1;
        }

        for (int i = candy.length - 1; i > 0; --i) {
            candy[i - 1] = ratings[i] < ratings[i - 1] ? Math.max(candy[i - 1], candy[i] + 1): candy[i - 1];
        }

        int minTotal = 0;
        for (int num: candy) {
            minTotal += num;
        }
        return minTotal;
    }

    public static void main(String[] args) {

    }
}
