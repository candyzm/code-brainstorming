package com.kouzengm;

import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 *
 * 206. 反转链表
 *
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 *
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1
 *
 * 输入：head = [1,2]
 * 输出：[2,1]
 *
 * 输入：head = []
 * 输出：[]
 */

public class Easy206 {
    public static void main(String[] args) {

    }

    public ListNode reverseList(ListNode head) {
       ListNode prev = null;
       ListNode cur = head;
        while (cur != null) {
            ListNode next = cur.next;
            cur.next = prev;
            prev = cur;
            cur = next;
        }
        return prev;
    }
}
