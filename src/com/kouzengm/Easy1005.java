package com.kouzengm;

import java.util.Arrays;

/**
 * @author 夕桐
 * @version 1.0
 * 1005.K次取反后最大化的数组和
 */

public class Easy1005 {
    public int largestSumAfterKNegations(int[] nums, int k) {
        Arrays.sort(nums);
        int sum = 0;
        for (int i = 0; i < nums.length; ++i) {
            if (nums[i] < 0 && k > 0) {
                nums[i] = -nums[i];
                --k;
            }
            sum += nums[i];
        }
        Arrays.sort(nums);
        return sum - (k % 2 == 0 ? 0: 2 * nums[0]);
    }
    public static void main(String[] args) {

    }
}
