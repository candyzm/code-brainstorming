package com.kouzengm;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * @author 夕桐
 * @version 1.0
 * 503.下一个更大元素II
 */

public class Mid503 {
    public static int[] nextGreaterElements(int[] nums) {
        int n = nums.length;
        int[] ans = new int[n];
        Arrays.fill(ans, -1);
        Deque<Integer> deque = new LinkedList<>();
        deque.addFirst(0);
        for (int i = 1; i < 2 * n; ++i) {
            while (!deque.isEmpty() && (nums[i % n] > nums[deque.peekFirst()])) {
                ans[deque.peekFirst()] = nums[i % n];
                deque.removeFirst();
            }
            deque.addFirst(i % n);
        }

        return ans;
    }
    public static void main(String[] args) {
        int[] num = new int[]{1, 2, 1};
        int[] res = Mid503.nextGreaterElements(num);
        for (int n: res) {
            System.out.println(n);
        }
    }
}
