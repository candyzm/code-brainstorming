package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 40. 组合总和II
 */

public class Mid40 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        combinationSum2Handler(candidates, target, 0, 0);
        return result;
    }

    public void combinationSum2Handler(int[] candidates, int target, int startIndex, int sum) {
        if (sum == target) {
            result.add(new ArrayList(path));
        }

        for (int i = startIndex; i < candidates.length; ++i) {
            if (sum + candidates[i] > target || (i > startIndex && candidates[i] == candidates[i - 1])) {
                continue;
            }
            path.add(candidates[i]);
            combinationSum2Handler(candidates, target, i + 1, sum + candidates[i]);
            path.remove(path.size() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
