package com.kouzengm;

import java.util.Arrays;

/**
 * @author 夕桐
 * @version 1.0
 * 977. 有序数组的平方
 *
 *给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。
 *
 * 示例 1：
 * 输入：nums = [-4,-1,0,3,10]
 * 输出：[0,1,9,16,100]
 * 解释：平方后，数组变为 [16,1,0,9,100]
 * 排序后，数组变为 [0,1,9,16,100]
 *
 * 示例 2：
 * 输入：nums = [-7,-3,2,3,11]
 * 输出：[4,9,9,49,121]
 *
 * 提示：
 *     1 <= nums.length <= 104
 *     -104 <= nums[i] <= 104
 *     nums 已按 非递减顺序 排序
 */

// 双指针法
public class Easy977_2 {
    public static void main(String[] args) {
        int[] nums = {-7,-3,2,3,11};
        int[] res = sortedSquares(nums);
        for (int re : res) {
            System.out.print(re + " ");
        }
    }

    public static int[] sortedSquares(int[] nums) {
        // 因为是非递减数组，所以该数组为递增数组，或有相同元素并排
        // 如 -2， -1， -1， 0， 1， 1， 2， 3
        int[] results = new int[nums.length];
        int p = 0;
        int q = nums.length - 1;
        int pos = nums.length - 1; // 逆序放进新开辟的数组，因为最小值可能在中间因此不能选择较小的数
        while (p <= q) {
            if (nums[p] * nums[p] > nums[q] * nums[q]) {
                results[pos--] = nums[p] * nums[p];
                ++p;
            } else {
                results[pos--] = nums[q] * nums[q];
                --q;
            }
        }
        return results;
    }
}
