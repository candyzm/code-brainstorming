package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 738.单调递增的数字
 */

public class Mid738 {
    public int monotoneIncreasingDigits(int n) {
        // 将数字转为字符数组
        char[] arr = String.valueOf(n).toCharArray();
        // 遍历
        int start = arr.length;
        for (int i = arr.length - 1; i > 0; --i) {
            if (arr[i - 1] > arr[i]) {
                --arr[i - 1];
                start = i;
            }
        }

        // 将后续数字修改为9
        for (int i = start; i < arr.length; ++i) {
            arr[i] = '9';
        }

        // 返回结果
        return Integer.parseInt(String.valueOf(arr));
    }
    public static void main(String[] args) {

    }
}
