package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 257. 二叉树的所有路径
 */

public class Easy257 {
    private List<String> res = new ArrayList<>();

    public List<String> binaryTreePaths(TreeNode root) {
        treePath(root, "");
        return res;
    }

    public void treePath(TreeNode root, String path) {
        if (root == null) return;
        StringBuilder sb = new StringBuilder(path);
        sb.append(root.val);
        // 叶子结点
        if (root.left == null && root.right == null) {
            // 将路径加入到答案
            res.add(sb.toString());
            return;
        }

        sb.append("->");
        treePath(root.left, sb.toString());
        treePath(root.right, sb.toString());

    }

    public static void main(String[] args) {

    }
}
