package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 45.跳跃游戏 II
 */

public class Mid45 {
    public int jump(int[] nums) {
        int cur = 0;
        int max = 0;
        int steps = 0;
        for (int i = 0; i < nums.length - 1; ++i) {
            max = Math.max(max, i + nums[i]);
            if (i == cur) {
                cur = max;
                ++steps;
            }
        }
        return steps;
    }
    public static void main(String[] args) {

    }
}
