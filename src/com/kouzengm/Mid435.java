package com.kouzengm;

import java.util.Arrays;

/**
 * @author 夕桐
 * @version 1.0
 * 435. 无重叠区间
 */

public class Mid435 {
    public int eraseOverlapIntervals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[0], b[0]));
        int res = 0;
        for (int i = 1; i < intervals.length; ++i) {
            if (intervals[i][0] < intervals[i - 1][1]) {
                ++res;
                intervals[i][1] = Math.min(intervals[i - 1][1], intervals[i][1]);
            }
        }
        return res;
    }
    public static void main(String[] args) {

    }
}
