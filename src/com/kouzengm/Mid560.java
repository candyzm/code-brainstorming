package com.kouzengm;

import java.util.HashMap;
import java.util.Map;

/**
 * 560. 和为 k 的子数组
 */
public class Mid560 {
    public int subarraySum(int[] nums, int k) {
        int ans = 0, pre = 0;
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        for (int i = 0; i < nums.length; ++i) {
            pre += nums[i];
            if (map.containsKey(pre - k)) {
                ans += map.get(pre - k);
            }
            map.put(pre, map.getOrDefault(pre, 0) + 1);
        }
        return ans;
    }
    public static void main(String[] args) {

    }
}
