package com.kouzengm;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author 夕桐
 * @version 1.0
 * 20. 有效的括号
 */

public class Easy20 {
    public boolean isValid(String s) {
        Deque<Character> deque = new LinkedList<>();
        int len = s.length();
        for (int i = 0; i < len; ++i) {
            if (s.charAt(i) == '{') {
                deque.push('}');
            } else if (s.charAt(i) == '(') {
                deque.push(')');
            } else if (s.charAt(i) == '[') {
                deque.push(']');
            } else if (deque.isEmpty() || s.charAt(i) != deque.peek()) {
                return false;
            } else {
                deque.pop();
            }
        }
        return deque.isEmpty();
    }
    public static void main(String[] args) {

    }
}
