package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 459.重复的子字符串
 */

public class Easy459_3 {
    public static void main(String[] args) {

    }

    public boolean repeatedSubstringPattern(String s) {
        // KMP 算法
        int[] next = getNext(s);
        int len = s.length();
        if (next[next.length - 1] != 0 && (len % (len - next[len - 1]) == 0)) {
            return true;
        }

        return false;
    }



    public int[] getNext(String s) {
        int[] next = new int[s.length()];
        next[0] = 0;
        int n = s.length();
        for (int i = 1, j = 0; i < n; ++i) {
            while (j > 0 && s.charAt(i) != s.charAt(j)) {
                j = next[j - 1];
            }

            if (s.charAt(i) == s.charAt(j)) {
                ++j;
            }

            next[i] = j;
        }

        return next;
    }
}
