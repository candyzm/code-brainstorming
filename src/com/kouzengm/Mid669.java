package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 669. 修剪二叉搜索树
 */

public class Mid669 {
    public TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) {
            return root;
        }

        if (root.val < low) {
            return trimBST(root.right, low, high);
        } else if (root.val > high) {
            return trimBST(root.left, low, high);
        } else {
            root.left = trimBST(root.left, low, root.val);
            root.right = trimBST(root.right, root.val, high);
            return root;
        }
    }
    public static void main(String[] args) {

    }
}
