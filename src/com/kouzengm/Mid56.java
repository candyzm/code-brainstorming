package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 56. 合并区间
 */

public class Mid56 {
    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> Integer.compare(a[0], b[0]));
        List<int[]> res = new ArrayList<>();
        res.add(intervals[0]);
        for (int i = 1; i < intervals.length; ++i) {
            if (intervals[i][0] <= res.get(res.size() - 1)[1]) {
                int left = Math.min(intervals[i][0], res.get(res.size() - 1)[0]);
                int right = Math.max(intervals[i][1], res.get(res.size() - 1)[1]);
                res.remove(res.size() - 1);
                res.add(new int[]{left, right});
                continue;
            }
            res.add(intervals[i]);
        }
        return res.toArray(new int[res.size()][]);
    }
    public static void main(String[] args) {

    }
}
