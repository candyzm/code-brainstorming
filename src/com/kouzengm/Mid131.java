package com.kouzengm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 131.分割回文串
 */

public class Mid131 {
    List<List<String>> result = new ArrayList<>();
    List<String> path = new LinkedList<>();
    public List<List<String>> partition(String s) {
        partitionHandler(s, 0);
        return result;
    }

    public void partitionHandler(String s, int startIndex) {
        if (startIndex >= s.length()) {
            result.add(new ArrayList(path));
            return;
        }

        for (int i = startIndex; i < s.length(); ++i) {
            String str = s.substring(startIndex, i + 1);
            if (isHuiWen(str)) {
                path.add(str);
            } else {
                continue;
            }
            partitionHandler(s, i + 1);
            path.remove(path.size() - 1);
        }
    }

    public boolean isHuiWen(String s) {
        int start = 0, end = s.length() - 1;
        for (int i = start, j = end; i < j; ++i, --j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {

    }
}
