package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 746. 使用最小花费爬楼梯
 */

public class Easy746 {
    public int minCostClimbingStairs(int[] cost) {
        int[] dp = new int[cost.length + 1];
        for (int i = 2; i <= cost.length; ++i) {
            dp[i] = Math.min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        }
        return dp[cost.length];
    }
    public static void main(String[] args) {

    }
}
