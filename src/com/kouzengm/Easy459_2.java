package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 459.重复的子字符串
 */

public class Easy459_2 {
    public static void main(String[] args) {

    }

    public boolean repeatedSubstringPattern(String s) {
        // 暴力求解
        int n = s.length();
        for (int i = 1; 2 * i <= n; ++i) {
            if (n % i == 0) {
                boolean flag = true;
                for (int j = i; j < n; ++j) {
                    if (s.charAt(j - i) != s.charAt(j)) {
                        flag = false;
                        break;
                    }
                }

                if (flag) {
                    return true;
                }
            }
        }

        return false;
    }
}
