package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 51. N皇后
 *
 */

public class Hard51 {
    List<List<String>> result = new ArrayList<>();
    public List<List<String>> solveNQueens(int n) {
        char[][] board = new char[n][n];
        // 填充空棋盘
        for (char[] c: board) {
            Arrays.fill(c, '.');
        }
        solveNQueensHandle(0, n, board);
        return result;
    }

    public void solveNQueensHandle(int row, int n, char[][] board) {
        // 结束条件
        if (row == n) {
            result.add(Array2List(board));
            return;
        }

        for (int col = 0; col < n; ++col) {
            if (isValid(row, col, n, board)) {
                board[row][col] = 'Q';
                solveNQueensHandle(row + 1, n ,board);
                board[row][col] = '.';
            }
        }
    }

    // 判断能否在该位置放皇后Q
    public boolean isValid(int row, int col, int n, char[][] board) {
        for (int i = 0; i < row; ++i) {
            if (board[i][col] == 'Q') {
                return false;
            }
        }

        for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; --i, --j) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }

        for (int i = row - 1, j = col + 1; i >= 0 && j < n; --i, ++j) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }

    // 将数组转换成列表
    public List Array2List(char[][] board) {
        List<String> list = new ArrayList<>();
        for (char[] c: board) {
            list.add(String.valueOf(c));
        }
        return list;
    }
    public static void main(String[] args) {

    }
}
