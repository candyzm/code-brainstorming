package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 54. 螺旋矩阵
 * <p>
 * 给你一个 m 行 n 列的矩阵 matrix ，请按照 顺时针螺旋顺序 ，返回矩阵中的所有元素。
 * <p>
 * 示例一:
 * 输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * 输出：[1,2,3,6,9,8,7,4,5]
 * <p>
 * 示例二:
 * 输入：matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
 * 输出：[1,2,3,4,8,12,11,10,9,5,6,7]
 */

public class Mid54 {
    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        List<Integer> ans = spiralOrder(matrix);
        for (Integer an : ans) {
            System.out.print(an + " ");
        }
    }

    public static List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> ans = new ArrayList<>();
        int m = matrix.length;
        int n = matrix[0].length;
        int top = 0, left = 0;
        int bottom = m - 1, right = n - 1;
        while (top <= bottom && left <= right) {
            // 顶部
            for (int j = left; j <= right; ++j) {
                ans.add(matrix[top][j]);
            }

            // 右列
            for (int i = top + 1; i <= bottom; ++i) {
                ans.add(matrix[i][right]);
            }

            if (top < bottom && left < right) {
                // 下面
                for (int j = right - 1; j >= left; --j) {
                    ans.add(matrix[bottom][j]);
                }

                // 左列
                for (int i = bottom - 1; i > top; --i) {
                    ans.add(matrix[i][left]);
                }
            }

            ++top;
            --right;
            ++left;
            --bottom;
        }
        return ans;
    }
}
