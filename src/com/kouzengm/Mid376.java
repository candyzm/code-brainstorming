package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 376. 摆动序列
 */

public class Mid376 {
    public int wiggleMaxLength(int[] nums) {
        if (nums.length <= 1) {
            return nums.length;
        }

        int preDiff = 0;
        int curDiff = 0;
        int count = 1;
        for (int i = 1; i < nums.length; ++i) {
            // 得到当前的差值
            curDiff = nums[i] - nums[i - 1];
            if ((curDiff > 0 && preDiff <= 0) || (curDiff < 0 && preDiff >= 0)) {
                ++count;
                preDiff = curDiff;
            }
        }
        return count;
    }
    public static void main(String[] args) {

    }
}
