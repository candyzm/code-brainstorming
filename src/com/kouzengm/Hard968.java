package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 968.监控二叉树
 */

public class Hard968 {
    int res = 0;
    public int minCameraCover(TreeNode root) {
        if (minCamera(root) == 0) {
            ++res;
        }
        return res;
    }

    public int minCamera(TreeNode node) {
        // 0 未覆盖 1 摄像头 2 已覆盖
        if (node == null) {
            return 2;
        }

        int left = minCamera(node.left);
        int right = minCamera(node.right);

        if (left == 2 && right == 2) {
            return 0;
        } else if (left == 0 || right == 0) {
            ++res;
            return 1;
        } else {
            return 2;
        }
    }
    public static void main(String[] args) {

    }
}
