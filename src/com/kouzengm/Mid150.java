package com.kouzengm;

import java.util.Stack;

/**
 * @author 夕桐
 * @version 1.0
 * 150. 逆波兰表达式求值
 */

public class Mid150 {
    public static void main(String[] args) {

    }
    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        int res = 0;
        for (String token : tokens) {
            if ("+".equals(token)) {
                res = stack.pop() + stack.pop();
                stack.push(res);
            } else if ("-".equals(token)) {
                res = -stack.pop() + stack.pop();
                stack.push(res);
            } else if ("*".equals(token)) {
                res = stack.pop() * stack.pop();
                stack.push(res);
            } else if ("/".equals(token)) {
                int m = stack.pop();
                int n = stack.pop();
                res = n / m;
                stack.push(res);
            } else {
                stack.push(Integer.valueOf(token));
            }
        }

        return stack.pop();
    }

}
