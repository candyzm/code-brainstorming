package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 367. 有效的完全平方数
 *
 * 给你一个正整数 num 。如果 num 是一个完全平方数，则返回 true ，否则返回 false 。
 * 完全平方数 是一个可以写成某个整数的平方的整数。换句话说，它可以写成某个整数和自身的乘积。
 * 不能使用任何内置的库函数，如  sqrt 。
 *
 *
 * 示例 1：
 * 输入：num = 16
 * 输出：true
 * 解释：返回 true ，因为 4 * 4 = 16 且 4 是一个整数。
 *
 * 示例 2：
 * 输入：num = 14
 * 输出：false
 * 解释：返回 false ，因为 3.742 * 3.742 = 14 但 3.742 不是一个整数。
 *
 * 1 <= num <= 231 - 1
 */

public class Easy367 {
    public static void main(String[] args) {
        int num = 2147395599;
        System.out.println(isPerfectSquare(num));
    }

    public static boolean isPerfectSquare(int num) {
        // 思路参考 x 的平方根
        int left = 1;
        int right = num;
        while (left <= right) {
            int mid = left + ((right - left) >> 2);
            if ((long) mid * mid == num) {
                return true;
            } else if ((long) mid * mid > num) {
                right = mid - 1;
            } else if ((long) mid * mid < num) {
                left = mid + 1;
            }
        }
        return false;
    }
}
