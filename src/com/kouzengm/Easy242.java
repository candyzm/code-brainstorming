package com.kouzengm;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 夕桐
 * @version 1.0
 * 242. 有效的字母异位词
 *
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 * 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。
 *
 *
 *
 * 示例 1:
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 *
 * 示例 2:
 * 输入: s = "rat", t = "car"
 * 输出: false
 *
 *
 *
 * 提示:
 *     1 <= s.length, t.length <= 5 * 104
 *     s 和 t 仅包含小写字母
 */

public class Easy242 {
    public static void main(String[] args) {

    }

    public boolean isAnagram(String s, String t) {
        return getMap(s).equals(getMap(t));

    }

    public Map<Character, Integer> getMap(String str) {
        Map<Character, Integer> cnt = new HashMap<>();
        for (int i = 0; i < str.length(); ++i) {
            char ch = str.charAt(i);
            cnt.put(ch, cnt.getOrDefault(ch, 0) + 1);
        }
        return cnt;
    }
}
