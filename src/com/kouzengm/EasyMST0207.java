package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 面试题 02.07 链表相交
 *
 * 给你两个单链表的头节点 headA 和 headB ，
 * 请你找出并返回两个单链表相交的起始节点。如果两个链表没有交点，返回 null 。
 */

public class EasyMST0207 {
    public static void main(String[] args) {

    }

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }
        // 统计 A 链表的长度
        int lenA = getLength(headA);
        // 统计 B 链表的长度
        int lenB = getLength(headB);
        // p 指向长度短的链表, q 指向长度长的链表
        ListNode slow = (lenA < lenB) ? headA : headB;
        ListNode fast = (slow == headA) ? headB : headA;

        // 让长度长的快指针先走
        for (int i = 0; i < Math.abs(lenA - lenB); ++i) {
            fast = fast.next;
        }

        // 两个指针一起走
        while (slow != null && fast != null) {
            // 注意是比较相同节点而不是值
            if (slow == fast) {
                return slow;
            }
            slow = slow.next;
            fast = fast.next;
        }

        return null;
    }

    public int getLength(ListNode head) {
        int len = 0;
        while (head != null) {
            ++len;
            head = head.next;
        }
        return len;
    }
}
