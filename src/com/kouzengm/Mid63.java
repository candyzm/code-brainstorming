package com.kouzengm;

/**
 * 63. 不同路径 II
 */
public class Mid63 {
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; ++i) {
            if (obstacleGrid[i][0] == 1) {
                break;
            }
            dp[i][0] = 1;
        }

        for (int i = 0; i < n; ++i) {
            if (obstacleGrid[0][i] == 1) {
                break;
            }
            dp[0][i] = 1;
        }

        for (int i = 1; i < m; ++i) {
            for (int j = 1; j < n; ++j) {
                int top = (obstacleGrid[i - 1][j] == 1) ? 0 : dp[i - 1][j];
                int left = (obstacleGrid[i][j - 1] == 1) ? 0 : dp[i][j - 1];
                dp[i][j] = top + left;
            }
        }

        return obstacleGrid[m - 1][n - 1] == 1 ? 0 : dp[m - 1][n - 1];
    }
    public static void main(String[] args) {

    }
}
