package com.kouzengm;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 夕桐
 * @version 1.0
 * 106.从中序与后序遍历序列构造二叉树
 */

public class Mid106 {
    private Map<Integer, Integer> map;
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        map = new HashMap<>();
        for (int i = 0; i < inorder.length; ++i) {
            map.put(inorder[i], i);
        }

        return findNode(inorder, 0, inorder.length, postorder, 0, postorder.length);
    }

    public TreeNode findNode(int[] inorder, int inStart, int inEnd, int[] postorder, int postStart, int postEnd) {
        if (inStart >= inEnd || postStart >= postEnd) {
            return null;
        }

        // 找到后序遍历中的最后一个元素在中序遍历的索引
        int rootIndex = map.get(postorder[postEnd - 1]);
        // 构造节点
        TreeNode node = new TreeNode(inorder[rootIndex]);
        // 保存中序左子树个数
        int numOfLeft = rootIndex - inStart;
        node.left = findNode(inorder, inStart, rootIndex, postorder, postStart, postStart + numOfLeft);
        node.right = findNode(inorder, rootIndex + 1, inEnd, postorder, postStart + numOfLeft, postEnd - 1);
        return node;
    }
    public static void main(String[] args) {

    }
}
