package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 763.划分字母区间
 */

public class Mid763 {
    public List<Integer> partitionLabels(String s) {
        List<Integer> res = new ArrayList<>();
        int[] flag = new int[26];
        char[] arr = s.toCharArray();
        for (int i = 0; i < arr.length; ++i) {
            flag[arr[i] - 'a'] = i;
        }
        int idx = 0;
        int last = -1;
        for (int i = 0; i < arr.length; ++i) {
            idx = Math.max(idx, flag[arr[i] - 'a']);
            if (i == idx) {
                res.add(idx - last);
                last = idx;
            }
        }
        return res;
    }
    public static void main(String[] args) {

    }
}
