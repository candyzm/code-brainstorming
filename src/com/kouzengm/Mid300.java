package com.kouzengm;

import java.util.Arrays;

/**
 * @author 夕桐
 * @version 1.0
 * 300.最长递增子序列
 */

public class Mid300 {
    public int lengthOfLIS(int[] nums) {
        if (nums.length <= 1) {
            return nums.length;
        }
        int[] dp = new int[nums.length];
        Arrays.fill(dp, 1);
        int res = 1;
        for (int i = 1; i < nums.length; ++i) {
           for (int j = 0; j < i; ++j) {
               if (nums[i] > nums[j]) {
                   dp[i] = Math.max(dp[i], dp[j] + 1);
               }
           }
           res = Math.max(res, dp[i]);
        }
        return dp[nums.length - 1];
    }

    public static void main(String[] args) {
        int[] nums = new int[]{4,10,4,3,8,9};
        Mid300 mid300 = new Mid300();
        int res = mid300.lengthOfLIS(nums);
        System.out.println(res + " ");
    }
}
