package com.kouzengm;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author 夕桐
 * @version 1.0
 * 739. 每日温度
 */

public class Mid739 {
    public int[] dailyTemperatures(int[] temperatures) {
        int[] res = new int[temperatures.length];
        Deque<Integer> deque = new LinkedList<>();
        deque.addFirst(0);
        for (int i = 1; i < temperatures.length; ++i) {
            while (!(deque.isEmpty()) && temperatures[i] > temperatures[deque.peekFirst()]) {
                res[deque.peekFirst()] = i - deque.peekFirst();
                deque.removeFirst();
            }
            deque.addFirst(i);
        }
        return res;
    }
    public static void main(String[] args) {

    }
}
