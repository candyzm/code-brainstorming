package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 108.将有序数组转换为二叉搜索树
 */

public class Easy108 {
    public TreeNode sortedArrayToBST(int[] nums) {
        return constructTree(nums, 0, nums.length - 1);
    }

    public TreeNode constructTree(int[] nums, int left, int right) {
        if (left > right) {
            return null;
        }

        int mid = left + (right - left) / 2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = constructTree(nums, left, mid - 1);
        root.right = constructTree(nums, mid + 1, right);
        return root;
    }
    public static void main(String[] args) {

    }
}
