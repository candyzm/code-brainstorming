package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 199. 二叉树的右视图
 */

public class Mid199_1 {
    public List<List<Integer>> res = new ArrayList<>();

    public static void main(String[] args) {

    }

    public List<Integer> rightSideView(TreeNode root) {
        // 递归
        List<Integer> ans = new ArrayList<>();
        checkFun01(root, 0);
        for(List<Integer> list: res) {
            ans.add(list.get(list.size()- 1));
        }
        return ans;
    }

    public void checkFun01(TreeNode node, int deep) {
        if (node == null) return;
        ++deep;
        if (res.size() < deep) {
            List<Integer> list = new ArrayList<>();
            res.add(list);
        }
        res.get(deep - 1).add(node.val);
        checkFun01(node.left, deep);
        checkFun01(node.right, deep);
    }
}
