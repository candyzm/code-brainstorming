package com.kouzengm;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author 夕桐
 * @version 1.0
 * 55. 跳跃游戏
 */

public class Mid55 {
    public boolean canJump(int[] nums) {
        int cover = 0;
        for (int i = 0; i <= cover; ++i) {
            cover = Math.max(cover, i + nums[i]);
            if (cover >= nums.length - 1) {
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {

    }
}
