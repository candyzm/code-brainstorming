package com.kouzengm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 78. 子集
 */

public class Mid78 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> subsets(int[] nums) {
        subsetsHandler(nums, 0);
        return result;
    }

    public void subsetsHandler(int[] nums, int startIndex) {
        result.add(new ArrayList(path));
        for (int i = startIndex; i < nums.length; ++i) {
            path.add(nums[i]);
            subsetsHandler(nums, i + 1);
            path.remove(path.size() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
