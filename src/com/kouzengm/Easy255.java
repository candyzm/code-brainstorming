package com.kouzengm;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author 夕桐
 * @version 1.0
 * 255. 用队列实现栈
 */
class MyStack {
    Queue<Integer> que;
    public MyStack() {
        que = new LinkedList<>();
    }

    public void push(int x) {
        que.offer(x);
        int queSize = que.size();
        while (queSize-- > 1) {
            que.offer(que.poll());
        }
    }

    public int pop() {
        return que.poll();
    }

    public int top() {
        return que.peek();
    }

    public boolean empty() {
        return que.isEmpty();
    }
}

public class Easy255 {
    public static void main(String[] args) {

    }
}
