package com.kouzengm;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * @author 夕桐
 * @version 1.0
 * 406.根据身高重建队列
 */

public class Mid406 {
    public int[][] reconstructQueue(int[][] people) {
        // 按身高排序
        Arrays.sort(people, (a, b) -> {
            if (a[0] == b[0]) {
                return a[1] - b[1];
            }
            return b[0] - a[0];
        });
        LinkedList<int[]> que = new LinkedList<>();
        for (int[] p: people) {
            que.add(p[1], p);
        }
        return que.toArray(new int[people.length][]);
    }
    public static void main(String[] args) {

    }
}
