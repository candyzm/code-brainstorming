package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 1047. 删除字符串中的所有相邻重复项
 */

public class Easy1047_2 {
    public static void main(String[] args) {

    }

    public String removeDuplicates(String s) {
        char[] arr = s.toCharArray();
        int top = -1;
        int n = s.length();
        for (int i = 0; i < n; ++i) {
            if (top == -1 || arr[top] != arr[i]) {
                arr[++top] = arr[i];
            } else {
                --top;
            }
        }
        return String.valueOf(arr, 0, top + 1);
    }
}
