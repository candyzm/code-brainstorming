package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 501.二叉搜索树中的众数
 */

public class Easy501_2 {
    private List<Integer> list = new ArrayList<>();
    private int baseNum, count, maxCount;
    public int[] findMode(TreeNode root) {
        dfs(root);
        int[] res = new int[list.size()];
        for (int i = 0; i < res.length; ++i) {
            res[i] = list.get(i);
        }
        return res;
    }

    public void dfs(TreeNode root) {
        if (root == null) return;
        dfs(root.left);
        getMaxCount(root.val);
        dfs(root.right);
    }

    public void getMaxCount(int curNum) {
        if (curNum == baseNum) {
            ++count;
        } else {
            baseNum = curNum;
            count = 1;
        }

        if (count == maxCount) {
            list.add(baseNum);
        }

        if (count > maxCount) {
            list.clear();
            list.add(baseNum);
            maxCount = count;
        }
    }
    public static void main(String[] args) {

    }
}
