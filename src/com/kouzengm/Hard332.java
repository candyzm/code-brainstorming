package com.kouzengm;

import java.util.*;

/**
 * @author 夕桐
 * @version 1.0
 * 332.重新安排行程
 */

public class Hard332 {
    Map<String, Map<String, Integer>> map;
    List<String> result;
    public List<String> findItinerary(List<List<String>> tickets) {
        map = new HashMap<>();
        result = new LinkedList<>();
        //1.遍历航班航线
        for (List<String> ticket: tickets) {
            Map<String, Integer> tmp;
            if (map.containsKey(ticket.get(0))) {
                tmp = map.get(ticket.get(0));
                tmp.put(ticket.get(1), tmp.getOrDefault(ticket.get(1), 0) + 1);
            } else {
                tmp = new TreeMap<>();
                tmp.put(ticket.get(1), 1);
            }
            map.put(ticket.get(0), tmp);
        }

        //2.进行回溯
        result.add("JFK");
        dfs(tickets.size());
        return result;
    }

    public boolean dfs(int ticketNum) {
        if (result.size() == ticketNum + 1) {
            return true;
        }
        String src = result.get(result.size() - 1);
        if (!map.containsKey(src)) {
            return false;
        }

        for (Map.Entry<String, Integer> target: map.get(src).entrySet()) {
            int count = target.getValue();
            if (count <= 0) {
                continue;
            }
            target.setValue(count - 1);
            result.add(target.getKey());
            if(dfs(ticketNum)) {
                return true;
            }
            target.setValue(count);
            result.remove(result.size() - 1);
        }

        return false;
    }
    public static void main(String[] args) {

    }
}
