package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 151.翻转字符串里的单词
 */

public class Mid151 {
    public static void main(String[] args) {
        String s = "     hello    word      ";
        String res = reverseWords(s);
        System.out.println(res);
    }

    public static String reverseWords(String s) {
        char[] arr = s.toCharArray();
        // 去除多余的空格
        StringBuilder sb = removeSpace(arr, 0, arr.length - 1);
        // 反转整个字符串
        int l = 0;
        int r = sb.length() - 1;
        reverse(sb, l, r);
        // 反转每个单词
        reverseWord(sb);

        return new String(sb);
    }

    public static StringBuilder removeSpace(char[] arr, int start, int end) {
        StringBuilder sb = new StringBuilder();
        while (start < end && arr[start] == ' ') {
            ++start;
        }

        while (start < end && arr[end] == ' ') {
            --end;
        }

        while (start <= end) {
            if (arr[start] != ' ') {
                sb.append(arr[start]);
            } else if (sb.charAt(sb.length() - 1) != ' ') {
                sb.append(arr[start]);
            }
            ++start;
        }

        return sb;
    }

    public static void reverse(StringBuilder sb, int l, int r) {
        while (l <= r) {
            char tmp = sb.charAt(l);
            sb.setCharAt(l, sb.charAt(r));
            sb.setCharAt(r, tmp);
            ++l;
            --r;
        }
    }

    public static void reverseWord(StringBuilder sb) {
        int start = 0;
        int end = sb.length() - 1;
        while (start <= end) {
            int cur = start;
            while (cur <= end && sb.charAt(cur) != ' ') {
                ++cur;
            }

            reverse(sb, start, cur - 1);

            start = cur + 1;
            ++cur;
        }

    }
}
