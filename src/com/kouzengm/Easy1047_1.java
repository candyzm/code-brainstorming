package com.kouzengm;

import java.util.Stack;

/**
 * @author 夕桐
 * @version 1.0
 * 1047. 删除字符串中的所有相邻重复项
 */

public class Easy1047_1 {
    public static void main(String[] args) {

    }

    public String removeDuplicates(String s) {
        int n = s.length();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < n; ++i) {
            char ch = s.charAt(i);
            if (stack.isEmpty()) {
                stack.push(ch);
                continue;
            }

            if (ch == stack.peek()) {
                stack.pop();
            } else {
                stack.push(ch);
            }
        }

        int len = stack.size();
        char[] arr = new char[len];
        while (!stack.isEmpty()) {
            arr[--len] = stack.pop();
        }

        return new String(arr);

    }
}
