package com.kouzengm;

import java.util.*;

/**
 * @author 夕桐
 * @version 1.0
 * 332.重新安排行程
 */

public class Hard332_2 {
    Map<String, PriorityQueue<String>> map;
    List<String> result;
    public List<String> findItinerary(List<List<String>> tickets) {
        map = new HashMap<>();
        result = new LinkedList<>();
        for (List<String> ticket: tickets) {
            String src = ticket.get(0), dest = ticket.get(1);
            if (!map.containsKey(src)) {
                map.put(src, new PriorityQueue<>());
            }
            map.get(src).offer(dest);
        }
        dfs("JFK");
        Collections.reverse(result);
        return result;
    }

    public void dfs(String src) {
        while (map.containsKey(src) && map.get(src).size() > 0) {
            String newSrc = map.get(src).poll();
            dfs(newSrc);
        }
        result.add(src);
    }
    public static void main(String[] args) {

    }
}
