package com.kouzengm;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 夕桐
 * @version 1.0
 * 76. 最小覆盖字串
 * 给你一个字符串 s 、一个字符串 t 。返回 s 中涵盖 t 所有字符的最小子串。如果 s 中不存在涵盖 t 所有字符的子串，则返回空字符串 "" 。
 *
 * 注意:
 *     对于 t 中重复字符，我们寻找的子字符串中该字符数量必须不少于 t 中该字符数量。
 *     如果 s 中存在这样的子串，我们保证它是唯一的答案。

 *
 * 示例 1：
 * 输入：s = "ADOBECODEBANC", t = "ABC"
 * 输出："BANC"
 * 解释：最小覆盖子串 "BANC" 包含来自字符串 t 的 'A'、'B' 和 'C'。
 *
 * 示例 2：
 * 输入：s = "a", t = "a"
 * 输出："a"
 * 解释：整个字符串 s 是最小覆盖子串。
 *
 * 示例 3:
 * 输入: s = "a", t = "aa"
 * 输出: ""
 * 解释: t 中两个字符 'a' 均应包含在 s 的子串中，
 * 因此没有符合条件的子字符串，返回空字符串。
 *

 * 提示：
 *     m == s.length
 *     n == t.length
 *     1 <= m, n <= 105
 *     s 和 t 由英文字母组成
 */

public class Hard76 {
    public static void main(String[] args) {
        String s = "ADOBECODEBANC", t = "ABC";
        System.out.println(minWindow(s, t));
    }

    public static String minWindow(String s, String t) {
        // 采用滑窗思想
        Map<Character, Integer> tCnt = new HashMap<>();
        // 将 t 字符串需要的字母及其个数放入 HashMap 中
        for (int right = 0; right < t.length(); ++right) {
            char ch = t.charAt(right);
            tCnt.put(ch, tCnt.getOrDefault(ch, 0) + 1);
        }
        Map<Character, Integer> sCnt = new HashMap<>();
        int left = 0;
        int valid = 0;
        int start = 0; // 记录起始位置
        int ans = Integer.MAX_VALUE;  // 记录最小的长度
        for (int right = 0; right < s.length(); ++right) {
            char ch = s.charAt(right);
            // 判断是否存在于 tCnt
            if(tCnt.containsKey(ch)) {
                sCnt.put(ch, sCnt.getOrDefault(ch, 0) + 1);
                // 若该字符存在于 tCnt，则比较个数是否相同
                if (sCnt.get(ch).equals(tCnt.get(ch))) {
                    ++valid;
                }
            }

            // 判断此时 sCnt 中是否包含了 tCnt 全部的字符，且每个字符对应的数量符合条件
            while (valid == tCnt.size()) {
                // 若符合则需要更新缩小窗口的起始位置和长度
                if (right - left < ans) {
                    start = left;
                    ans = right - left;
                }

                char c = s.charAt(left);
                if (tCnt.containsKey(c)) {
                    sCnt.put(c, sCnt.get(c) - 1);
                    if (sCnt.get(c) < tCnt.get(c)) {
                        --valid;
                    }
                }
                ++left;
            }

        }
        return ans == Integer.MAX_VALUE ? "" : s.substring(start, start + ans + 1);
    }
}
