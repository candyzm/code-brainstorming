package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 242. 有效的字母异位词
 *
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 * 注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。
 *
 *
 *
 * 示例 1:
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 *
 * 示例 2:
 * 输入: s = "rat", t = "car"
 * 输出: false
 *
 *
 *
 * 提示:
 *     1 <= s.length, t.length <= 5 * 104
 *     s 和 t 仅包含小写字母
 */

// 哈希表 数组
public class Easy242_3 {
    public static void main(String[] args) {

    }

    public boolean isAnagram(String s, String t) {
        // 数组
        if (s.length() != t.length()) {
            return false;
        }
        int[] nums = new int[26];
        for (int i = 0; i < s.length(); ++i) {
            char ch = s.charAt(i);
            nums[ch - 'a']++;
        }

        for (int i = 0; i < t.length(); ++i) {
            char ch = t.charAt(i);
            nums[ch - 'a']--;
            if (nums[ch - 'a'] < 0) {
                return false;
            }
        }

        return true;
    }
}
