package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 37. 解数独
 */

public class Hard37 {
    public void solveSudoku(char[][] board) {
        solveSudokuHandle(0, board);
    }

    public boolean solveSudokuHandle(int row, char[][] board) {
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                if (!(board[i][j] == '.')) {
                    continue;
                }
                for (char k = '1'; k <= '9'; ++k) {
                    if (isValid(i, j, k, board)) {
                        board[i][j] = k;
                        if (solveSudokuHandle(row + 1, board)) {
                            return true;
                        }
                        board[i][j] = '.';
                    }
                }
                return false;
            }
        }
        return true;
    }

    public boolean isValid(int row, int col, char c, char[][] board) {
        for (int j = 0; j < 9; ++j) {
            if (board[row][j] == c) {
                return false;
            }
        }

        for (int i = 0; i < 9; ++i) {
            if (board[i][col] == c) {
                return false;
            }
        }

        int start = (row / 3) * 3;
        int end = (col / 3) * 3;
        for (int i = start; i < start + 3; ++i) {
            for (int j = end; j < end + 3; ++j) {
                if (board[i][j] == c) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {

    }
}
