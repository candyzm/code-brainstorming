package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 704. 二分查找
 * 给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target，
 * 写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
 *
 * 示例 1:
 * 输入: nums = [-1, 0, 3, 5, 9, 12], target = 9
 * 输出: 4
 * 解释: 9 出现在 nums 中并且下标为 4
 *
 * 示例 2:
 * 输入: nums = [-1, 0, 3, 5, 9, 12], target = 2
 * 输出: -1
 * 解释: 2 不存在 nums 中因此返回 -1
 */

public class Easy704 {
    public static void main(String[] args) {
        int[] nums = {-1, 0, 3, 5, 9, 12};
        int target = 2;
        int res = binarySearch(nums, target);
        System.out.println("index= " + res);
    }

    // 编写二分查找
    public static int binarySearch(int[] nums, int target) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        int left = 0;
        int right = nums.length - 1;
        // 到底是 < 还是 <= 要注意!! 左闭右闭 left == right 依然有效
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] > target) {
                right = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
