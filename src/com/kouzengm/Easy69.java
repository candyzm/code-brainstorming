package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 69. x 的平方根
 *
 * 给你一个非负整数 x ，计算并返回 x 的 算术平方根 。
 * 由于返回类型是整数，结果只保留整数部分 ，小数部分将被舍去 。
 * 注意：不允许使用任何内置指数函数和算符，例如 pow(x, 0.5) 或者 x ** 0.5 。
 *
 * 示例 1：
 * 输入：x = 4
 * 输出：2
 *
 * 示例 2：
 * 输入：x = 8
 * 输出：2
 * 解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。
 *
 * 提示：
 *
 *     0 <= x <= 231 - 1
 */

public class Easy69 {
    public static void main(String[] args) {
        int x = 2147395599;
        System.out.println(mySqrt(x));
    }
    public static int mySqrt(int x) {
        int left = 0;
        int right = x;
        int mid = 0;
        while (left <= right) {
            mid = left + ((right - left) >> 1);
            // 这里要考虑 mid * mid 超过 int 最大表示整数的情况
            if ((long) mid * mid > x) {
                right = mid - 1;
            } else if ((long) mid * mid < x) {
                left = mid + 1;
            } else {
                return mid;
            }
        }
        // 返回 right 即可，是因为无论是 right, left, _ 还是 _, right, left 的
        // 情况 right 都符合向下取整的要求
        return right;
    }
}
