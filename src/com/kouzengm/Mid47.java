package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 47.全排列 II
 */

public class Mid47 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> permuteUnique(int[] nums) {
        // 对原数组进行排序，方便后续对同一层元素是否使用过的判断
        Arrays.sort(nums);
        boolean[] used = new boolean[nums.length];
        permuteUniqueHandler(nums, used);
        return result;
    }

    public void permuteUniqueHandler(int[] nums, boolean[] used) {
        // 当path的元素数量等于nums长度即终止
        if (path.size() == nums.length) {
            result.add(new ArrayList(path));
            return;
        }

        for (int i = 0; i < nums.length; ++i) {
            // 对同一层元素进行去重判断
            if (i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false) {
                continue;
            }
            // 当 used[i] == true 时也跳过这一次循环
            if (used[i]) {
                continue;
            }
            path.add(nums[i]);
            used[i] = true;
            permuteUniqueHandler(nums, used);
            // 回溯
            used[i] = false;
            path.remove(path.size() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
