package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 122.买卖股票的最佳时机 II
 */

public class Mid122 {
    public int maxProfit(int[] prices) {
        if (prices.length <= 1) {
            return 0;
        }

        int result = 0;
        for (int i = 1; i < prices.length; ++i) {
            int profit = prices[i] - prices[i - 1];
            if (profit > 0) {
                result += profit;
            }
        }
        return result;
    }
    public static void main(String[] args) {

    }
}
