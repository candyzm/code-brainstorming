package com.kouzengm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 216.组合总和III
 */

public class Mid216 {
    List<List<Integer>> result = new ArrayList<>();
    LinkedList<Integer> path = new LinkedList<>();
    public List<List<Integer>> combinationSum3(int k, int n) {
        combinationSumHelper(k, n, 1, 0);
        return result;
    }

    public void combinationSumHelper(int k, int n, int startIndex, int sum) {
        if (sum > n) {
            return;
        }

        if (path.size() == k && sum == n) {
            result.add(new ArrayList(path));
            return;
        }

        for (int i = startIndex; i <= 9 - (k - path.size()) + 1; ++i) {
            path.add(i);
            combinationSumHelper(k, n, i + 1, sum + i);
            path.removeLast();
        }
    }
    public static void main(String[] args) {

    }
}
