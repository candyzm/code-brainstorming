package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 134. 加油站
 */

public class Mid134 {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int cur = 0;
        int totle = 0;
        int start = 0;
        for (int i = 0; i < gas.length; ++i) {
            int use = gas[i] - cost[i];
            cur += use;
            totle += use;
            if (cur < 0) {
                cur = 0;
                start = i + 1;
            }
        }
        if (totle < 0) return -1;
        return start;
    }
    public static void main(String[] args) {

    }
}
