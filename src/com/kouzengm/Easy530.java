package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 530.二叉搜索树的最小绝对差
 */

public class Easy530 {
    private TreeNode preNode = null;
    private int res = Integer.MAX_VALUE;
    public int getMinimumDifference(TreeNode root) {
        getMinNum(root);
        return res;
    }

    public void getMinNum(TreeNode root) {
        if (root == null) return;
        getMinNum(root.left);
        if (preNode != null) {
            res = Math.min(res, root.val - preNode.val);
        }
        preNode = root;
        getMinNum(root.right);
    }
    public static void main(String[] args) {

    }
}
