package com.kouzengm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 */

public class Mid17 {
    private List<String> result = new ArrayList<>();
    private String[] mapping = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    private StringBuilder sb = new StringBuilder();
    public List<String> letterCombinations(String digits) {
        if (digits == null || "".equals(digits)) {
            return result;
        }
        letterCombinations(digits, 0);
        return result;
    }

    public void letterCombinations(String digits, int num) {
        if (num == digits.length()) {
            result.add(sb.toString());
            return;
        }
        // 获得 num 对应的字符串
        String str = mapping[(digits.charAt(num) - '0')];
        // 遍历
        for (int i = 0; i < str.length(); ++i) {
            sb.append(str.charAt(i));
            letterCombinations(digits, num + 1);
            sb.deleteCharAt(sb.length() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
