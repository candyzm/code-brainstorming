package com.kouzengm;

import java.util.Scanner;

/**
 * @author 夕桐
 * @version 1.0
 * 55. 右旋字符串
 */

public class KamaCoder55 {
    public static void main(String[] agrs) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        String s = sc.next();
        StringBuilder sb = new StringBuilder(s);
        int n = s.length();
        KamaCoder55 main = new KamaCoder55();
        // 先旋转 0~n-k-1 的字符串
        main.reverse(sb, 0, n - k - 1);

        // 在旋转 n-k ~ n 的字符串
        main.reverse(sb, n - k, n - 1);

        // 最后字符串整体旋转
        main.reverse(sb, 0, n - 1);

        // 输出
        System.out.println(sb);
    }

    public void reverse(StringBuilder sb, int start, int end) {
        while (start <= end) {
            char tmp = sb.charAt(start);
            sb.setCharAt(start++, sb.charAt(end));
            sb.setCharAt(end--, tmp);
        }
    }
}
