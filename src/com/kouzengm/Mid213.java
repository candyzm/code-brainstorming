package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 213.打家劫舍II
 */

public class Mid213 {
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        int len = nums.length;
        return Math.max(robHandle(nums, 1, len), robHandle(nums, 2, len + 1));
    }

    public int robHandle(int[] nums, int start, int end) {
        int[] dp = new int[nums.length + 1];
        // dp[i] 表示到第 i 个房间为止偷窃到的最高金额为 dp[i]
        // dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i])
        if (start == 2) {
            dp[start - 1] = 0;
        }
        dp[start] = nums[start - 1];
        for (int i = start + 1; i <= end - 1; ++i) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i - 1]);
        }
        return dp[end - 1];
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2, 3, 2};
        Mid213 mid213 = new Mid213();
        int rob = mid213.rob(nums);
        System.out.println(rob);
    }
}
