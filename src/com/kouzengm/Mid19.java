package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 19. 删除链表的倒数第 n 个节点
 *
 * 示例 1：
 * 输入：head = [1,2,3,4,5], n = 2
 * 输出：[1,2,3,5]
 *
 * 示例 2：
 * 输入：head = [1], n = 1
 * 输出：[]
 *
 * 示例 3：
 * 输入：head = [1,2], n = 1
 * 输出：[1]
 */

public class Mid19 {
    public static void main(String[] args) {

    }

    public ListNode removeNthFromEnd(ListNode head, int n) {
        // 统计链表元素个数
        int size = 0;
        ListNode cur = head;
        while (cur != null) {
            ++size;
            cur = cur.next;
        }

        // 找到待删除元素的前驱节点
        ListNode dummy = new ListNode();
        dummy.next = head;
        cur = dummy;
        for (int i = 1; i < size - n + 1; ++i) {
            cur = cur.next;
        }
        cur.next = cur.next.next;
        return dummy.next;
    }
}
