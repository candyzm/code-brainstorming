package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 701.二叉搜索树中的插入操作
 */

public class Mid701 {
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }

        TreeNode res = root;
        while (res != null) {
            if (res.val > val) {
                if (res.left == null) {
                    res.left = new TreeNode(val);
                    break;
                } else {
                    res = res.left;
                }
            } else {
                if (res.right == null) {
                    res.right = new TreeNode(val);
                    break;
                } else {
                    res = res.right;
                }
            }
        }

        return root;
    }
    public static void main(String[] args) {

    }
}
