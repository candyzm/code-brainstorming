package com.kouzengm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 夕桐
 * @version 1.0
 * 501.二叉搜索树中的众数
 */

public class Easy501_1 {
    private Map<Integer, Integer> map = new HashMap<>();
    private List<Integer> index = new ArrayList<>();
    private int max = Integer.MIN_VALUE;

    public int[] findMode(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        getMap(root);
        for (Integer key : map.keySet()) {
            if(map.get(key) == max) {
                res.add(key);
            }
        }

        return res.stream().mapToInt(Integer::valueOf).toArray();
    }

    public void getMap(TreeNode root) {
        if (root == null)
            return;
        map.put(root.val, map.getOrDefault(root.val, 0) + 1);
        max = Math.max(max, map.get(root.val));
        getMap(root.left);
        getMap(root.right);
    }
    public static void main(String[] args) {

    }
}
