package com.kouzengm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 46. 全排列
 */

public class Mid46 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> permute(int[] nums) {
        boolean[] used = new boolean[nums.length];
        permuteHandler(nums, used);
        return result;
    }

    public void permuteHandler(int[] nums, boolean[] used) {
        if (path.size() == nums.length) {
            result.add(new ArrayList(path));
            return;
        }
        for (int i = 0; i < nums.length; ++i) {
            if (used[i]) {
                continue;
            }
            path.add(nums[i]);
            used[i] = true;
            permuteHandler(nums, used);
            path.remove(path.size() - 1);
            used[i] = false;
        }
    }

    public static void main(String[] args) {

    }
}
