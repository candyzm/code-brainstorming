package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 53. 最大子序和
 */

public class Mid53 {
    public int maxSubArray(int[] nums) {
        if (nums.length <= 1) {
            return nums[0];
        }

        int sum = Integer.MIN_VALUE;
        int count = 0;
        for (int i = 0; i < nums.length; ++i) {
            count += nums[i];
            sum = Math.max(sum, count);
            if (count < 0) {
                count = 0;
            }
        }
        return sum;
    }
    public static void main(String[] args) {

    }
}
