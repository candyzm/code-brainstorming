package com.kouzengm;


/**
 * @author 夕桐
 * @version 1.0
 * 59. 螺旋矩阵 II
 *
 * 给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。
 *
 * 1 -> 2 -> 3
 *
 * 8 -> 9    4
 *
 * 7 <- 6 <- 5
 *
 * 示例 1：
 * 输入：n = 3
 * 输出：[[1,2,3],[8,9,4],[7,6,5]]
 *
 * 示例 2：
 * 输入：n = 1
 * 输出：[[1]]
 *

 * 提示：
 *     1 <= n <= 20
 */

public class Mid59 {
    public static void main(String[] args) {
        int n = 4;
    }
    public static int[][] generateMatrix(int n) {
        int nums[][] = new int[n][n];
        int startX = 0, startY = 0; // 记录起始点
        int loop = 1, count = 1; // 记录圈数和矩阵中的值
        int i = 0, j = 0; // 记录行和列
        int offset = 1;

        while (loop <= n/ 2) {
            // 顶部
            for (j = startY; j < n - offset; ++j) {
                nums[startX][j] = count++;
            }

            // 右列
            for (i = startX; i < n - offset; ++i) {
                nums[i][j] = count++;
            }

            // 下面
            for (; j > startX; --j) {
                nums[i][j] = count++;
            }

            // 左列
            for (; i > startY; --i) {
                nums[i][j] = 0;
            }

            ++startX;
            ++startY;
            ++loop;
            ++offset;
        }

        if (n % 2 == 1) {
            nums[startX][startY] = count;
        }
        return nums;
    }


}
