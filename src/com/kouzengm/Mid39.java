package com.kouzengm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 39. 组合总和
 */

public class Mid39 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        combinationSumHandler(candidates, target, 0, 0);
        return result;
    }

    public void combinationSumHandler(int[] candidates, int target, int startIndex, int sum) {
        if (sum == target) {
            result.add(new ArrayList(path));
            return;
        }

        for (int i = startIndex; i < candidates.length; ++i) {
            if (sum + candidates[i] > target) {
                continue;
            }
            path.add(candidates[i]);
            combinationSumHandler(candidates, target, i, sum + candidates[i]);
            path.remove(path.size() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
