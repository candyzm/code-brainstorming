package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * LCR 146. 螺旋遍历二维数组
 * <p>
 * 螺旋遍历：从左上角开始，按照 向右、向下、向左、向上 的顺序 依次 提取元素，然后再进入内部一层重复相同的步骤，直到提取完所有元素。
 * <p>
 * <p>
 * 示例 1：
 * 输入：array = [[1,2,3],[8,9,4],[7,6,5]]
 * 输出：[1,2,3,4,5,6,7,8,9]
 * <p>
 * 示例 2：
 * 输入：array  = [[1,2,3,4],[12,13,14,5],[11,16,15,6],[10,9,8,7]]
 * 输出：[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
 * <p>
 * <p>
 * <p>
 * 限制：
 * <p>
 * 0 <= array.length <= 100
 * 0 <= array[i].length <= 100
 */

public class EasyLCR146 {
    public static void main(String[] args) {
        int[][] array = {{1, 2, 3, 4}, {12, 13, 14, 5}, {11, 16, 15, 6}, {10, 9, 8, 7}};
        spiralArray(array);
    }

    public static int[] spiralArray(int[][] array) {
        // 边界判断
        if (array == null || array.length == 0 || array[0].length == 0) {
            return new int[0];
        }
        int rows = array.length;
        int cols = array[0].length;
        int[] nums = new int[rows * cols];
        int index = 0;
        int top = 0, bottom = rows - 1;
        int left = 0, right = cols - 1;
        while (top <= bottom && left <= right) {
            // 顶部
            for (int j = left; j <= right; ++j) {
                nums[index++] = array[top][j];
            }

            // 右列
            for (int i = top + 1; i <= bottom; ++i) {
                nums[index++] = array[i][right];
            }

            if (top < bottom && left < right) {
                // 下面
                for (int j = right - 1; j >= left; --j) {
                    nums[index++] = array[bottom][j];
                }

                // 左列
                for (int i = bottom - 1; i >= top + 1; --i) {
                    nums[index++] = array[i][left];
                }
            }
            ++top;
            --bottom;
            ++left;
            --right;

        }
        return nums;
    }
}
