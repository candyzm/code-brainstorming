package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 235. 二叉搜索树的最近公共祖先
 */

public class Mid235 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        TreeNode res = root;
        while (true) {
            if (p.val < res.val && q.val < res.val) {
                res = lowestCommonAncestor(res.left, p, q);
            } else if (p.val > res.val && q.val > res.val) {
                res = lowestCommonAncestor(res.right, p, q);
            } else {
                break;
            }
        }

        return res;
    }
    public static void main(String[] args) {

    }
}
