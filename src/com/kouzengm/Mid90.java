package com.kouzengm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 夕桐
 * @version 1.0
 * 90.子集II
 */

public class Mid90 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        subsetsHandler(nums, 0);
        return result;
    }

    public void subsetsHandler(int[] nums, int startIndex) {
        result.add(new ArrayList(path));
        for (int i = startIndex; i < nums.length; ++i) {
            if (i > startIndex && nums[i] == nums[i - 1]) {
                continue;
            }
            path.add(nums[i]);
            subsetsHandler(nums, i + 1);
            path.remove(path.size() - 1);
        }
    }

    public static void main(String[] args) {

    }

}
