package com.kouzengm;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author 夕桐
 * @version 1.0
 * 232. 用栈实现队列
 */
class MyQueue {
    Deque<Integer> inStack;
    Deque<Integer> outStack;
    public MyQueue() {
        inStack = new ArrayDeque<>();
        outStack = new ArrayDeque<>();
    }

    public void push(int x) {
        inStack.push(x);
    }

    public int pop() {
        // 如果stackOut为空，那么将stackIn中的元素全部放到stackOut中
        if (outStack.isEmpty()) {
            dumpstackIn();
        }

        return outStack.pop();
    }

    public int peek() {
        if (outStack.isEmpty()) {
            dumpstackIn();
        }

        return outStack.peek();
    }

    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    public void dumpstackIn() {
        while (!inStack.isEmpty()) {
            outStack.push(inStack.pop());
        }
    }
}
public class Easy232 {
}
