package com.kouzengm;

/**
 * @author 夕桐
 * @version 1.0
 * 513.找树左下角的值
 */

public class Mid513 {
    public int maxDeep = -1;
    public int value = 0;
    public int findBottomLeftValue(TreeNode root) {
        value = root.val;
        findLeftLeafNode(root, 0);
        return value;
    }

    public void findLeftLeafNode(TreeNode root, int deep) {
        if (root == null) return;
        if (root.left == null && root.right == null) {
            if (deep > maxDeep) {
                value = root.val;
                maxDeep = deep;
            }
        }

        if (root.left != null) {
            findLeftLeafNode(root.left, deep + 1);
        }

        if (root.right != null) {
            findLeftLeafNode(root.right, deep + 1);
        }
    }
    public static void main(String[] args) {

    }
}
