package com.kouzengm;

import java.util.*;

/**
 * @author 夕桐
 * @version 1.0
 * 491.递增子序列
 */

public class Mid491 {
    List<List<Integer>> result = new ArrayList<>();
    List<Integer> path = new LinkedList<>();
    public List<List<Integer>> findSubsequences(int[] nums) {
        findSubsequencesHandler(nums, 0);
        return result;
    }

    public void findSubsequencesHandler(int[] nums, int startIndex) {
        if (path.size() >= 2) {
            result.add(new ArrayList(path));
        }
        Set<Integer> set = new HashSet<>();
        for (int i = startIndex; i < nums.length; ++i) {
            if (i > startIndex && nums[i] == nums[i - 1]) {
                continue;
            }
            if (!path.isEmpty() && path.get(path.size() - 1) > nums[i] || set.contains(nums[i])) {
                continue;
            }
            set.add(nums[i]);
            path.add(nums[i]);
            findSubsequencesHandler(nums, i + 1);
            path.remove(path.size() - 1);
        }
    }
    public static void main(String[] args) {

    }
}
